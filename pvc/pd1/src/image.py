"""Computer vision module, numpy and argparser"""
import sys
import numpy as np
import cv2


POS_NEW = []
POS_OLD = []


def print_usage():
    """Indicates how to run the program"""
    print('Program should run as follow: \n')
    print('python3 image.py image_file_path\n')


def on_mouse_click(event, pos_x, pos_y, flags, param):
    """Callback to capture mouse click on image"""
    if event == cv2.EVENT_LBUTTONDOWN:
        POS_NEW.clear()
        POS_NEW.append((pos_y, pos_x))


def calc_abs_dist(img):
    """Calculates absolute pixel difference when image has just one channel
        and returns image with pixels where difference is less than 13 colored red"""
    dist = np.ones(img.shape) * img
    dist = (dist - img[POS_NEW[0][0], POS_NEW[0][1]])
    red_array = (abs(dist) < 13) * 255
    img = img * (abs(dist) > 13)
    result = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
    result[:, :, 2] += np.uint8(red_array)
    return result


def calc_euc_dist(img):
    """Calculates euclidian distance when image has three channels
        and returns image with pixels where difference is less than 13 colored red"""
    dist = np.ones(img.shape) * img
    dist = dist[:, :, :dist.ndim] - \
        dist[POS_NEW[0][0], POS_NEW[0][1], :dist.ndim]
    dist = np.power(dist, 2)
    dist = np.sqrt(dist[:, :, 0] + dist[:, :, 1] + dist[:, :, 2])
    red_array = (dist < 13) * 255
    img = img * (dist > 13)[..., None]
    img[:, :, 2] += np.uint8(red_array)
    return img


def main():
    """Opens image and colors it based on pixel difference value"""
    if len(sys.argv) == 2:
        original_img = cv2.imread(sys.argv[1], cv2.IMREAD_ANYCOLOR)
        if original_img.any():
            cv2.namedWindow('Original image')
            cv2.imshow('Original image', original_img)
            cv2.setMouseCallback('Original image', on_mouse_click)
            while 1:
                if POS_NEW != POS_OLD:
                    if original_img.ndim == 3:
                        cv2.imshow('Colored image',
                                   calc_euc_dist(original_img))
                        print('PIXEL COORDINATES: x = {x}, y={y}'.format(
                            x=POS_NEW[0][1], y=POS_NEW[0][0]))
                        print('PIXEL VALUES: \033[0;31mRED: {red} \033[0;32mGREEN: {green}  \033[0;34mBLUE: {blue}\033[0m'.format(
                            red=original_img[POS_NEW[0][0], POS_NEW[0][1], 2],
                            green=original_img[POS_NEW[0]
                                               [0], POS_NEW[0][1], 1],
                            blue=original_img[POS_NEW[0][0], POS_NEW[0][1], 0],))
                    else:
                        print('PIXEL COORDINATES: x = {x}, y={y}'.format(
                            x=POS_NEW[0][1], y=POS_NEW[0][0]))
                        cv2.imshow('Colored image',
                                   calc_abs_dist(original_img))
                        print('PIXEL VALUE: {intensity}'.format(
                            intensity=original_img[POS_NEW[0][0], POS_NEW[0][1]]))

                    POS_OLD.clear()
                    POS_OLD.append(POS_NEW[0])
                if cv2.waitKey(33) & 0xFF == ord('q'):
                    break
        else:
            print('Error loading image')
    else:
        print_usage()


if __name__ == "__main__":
    main()

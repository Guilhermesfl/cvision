"""Computer vision module, numpy and argparser"""
import sys
import numpy as np
import cv2


POS_NEW = []
POS_OLD = []


def print_usage():
    """Indicates how to run the program"""
    print('Program should run as follow: \n')
    print('python3 webcam.py image_file_path\n')


def on_mouse_click(event, pos_x, pos_y, flags, param):
    """Callback to capture mouse click on image"""
    if event == cv2.EVENT_LBUTTONDOWN:
        POS_NEW.clear()
        POS_NEW.append((pos_y, pos_x))


def calc_abs_dist(img):
    """Calculates absolute pixel difference when image has just one channel
        and returns image with pixels where difference is less than 13 colored red"""
    if POS_NEW != POS_OLD:
        print('PIXEL COORDINATES: x = {x}, y={y}'.format(
            x=POS_NEW[0][1], y=POS_NEW[0][0]))
        print('PIXEL VALUE: {intensity}'.format(
            intensity=img[POS_NEW[0][0], POS_NEW[0][1]]))
        POS_OLD.clear()
        POS_OLD.append(POS_NEW[0])
    dist = np.ones(img.shape) * img
    dist = (dist - img[POS_NEW[0][0], POS_NEW[0][1]])
    red_array = (abs(dist) < 13) * 255
    img = img * (abs(dist) > 13)
    result = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
    result[:, :, 2] += np.uint8(red_array)
    return result


def calc_euc_dist(img):
    """Calculates euclidian distance when image has three channels
        and returns image with pixels where difference is less than 13 colored red"""
    if POS_NEW != POS_OLD:
        print('PIXEL COORDINATES: x = {x}, y={y}'.format(
            x=POS_NEW[0][1], y=POS_NEW[0][0]))
        print('PIXEL VALUES: \033[0;31mRED: {red} \033[0;32mGREEN: {green} \033[0;34mBLUE: {blue}\033[0m'.format(
            red=img[POS_NEW[0]
                    [0], POS_NEW[0][1], 2],
            green=img[POS_NEW[0]
                      [0], POS_NEW[0][1], 1],
            blue=img[POS_NEW[0][0], POS_NEW[0][1], 0],))
        POS_OLD.clear()
        POS_OLD.append(POS_NEW[0])

    dist = np.ones(img.shape) * img
    dist = dist[:, :, :dist.ndim] - \
        dist[POS_NEW[0][0], POS_NEW[0][1], :dist.ndim]
    dist = np.power(dist, 2)
    dist = np.sqrt(dist[:, :, 0] + dist[:, :, 1] + dist[:, :, 2])
    red_array = (dist < 13) * 255
    img = img * (dist > 13)[..., None]
    img[:, :, 2] += np.uint8(red_array)
    return img


def main():
    """Opens video and colors it based on pixel difference value"""
    if len(sys.argv) == 2:
        while 1:
            cap = cv2.VideoCapture(sys.argv[1])

            if cap.isOpened():
                while cap.isOpened():
                    cv2.namedWindow('video')
                    ret, frame = cap.read()
                    cv2.setMouseCallback('video', on_mouse_click)
                    if ret:
                        if POS_NEW:
                            if frame.ndim == 3:
                                frame = calc_euc_dist(frame)
                            else:
                                frame = calc_abs_dist(frame)
                        cv2.imshow('video', frame)
                        if cv2.waitKey(33) & 0xFF == ord('q'):
                            print('End program')
                            cap.release()
                            cv2.destroyAllWindows()
                            exit()
                            break
                    else:
                        break
            else:
                cap.release()
                cv2.destroyAllWindows()
                print('error loading video file')
    else:
        print_usage()

    cap.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    main()
